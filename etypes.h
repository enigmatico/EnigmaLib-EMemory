#ifndef __ENIGMA_TYPES__
#define __ENIGMA_TYPES__

/* Taken from wordsize.h */
#if defined __x86_64__ && !defined __ILP32__
# define __WORDSIZE        64
#else
# define __WORDSIZE        32
#define __WORDSIZE32_SIZE_ULONG                0
#define __WORDSIZE32_PTRDIFF_LONG        0
#endif
#ifdef __x86_64__
# define __WORDSIZE_TIME64_COMPAT32        1
/* Both x86-64 and x32 use the 64-bit system call interface.  */
# define __SYSCALL_WORDSIZE                64
#else
# define __WORDSIZE_TIME64_COMPAT32        0
#endif

/* Signed types
 * (s = signed, u = unsigned, 8/16/32/64 number of bits )*/
typedef signed char s8;
typedef signed short int s16;
typedef signed int s32;

/* Unsigned types */
typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;

/* 64-bit types */
#if __WORDSIZE == 64
typedef signed long int s64;
typedef unsigned long int u64;
#else
typedef signed long long int s64;
typedef unsigned long long int u64;
#endif

/* snv and unv have a size equal to the CPU architecture size 
 * (s = signed, u = unsigned, n = numeric, v = value )*/
#if __WORDSIZE==32
typedef s32 snv;
typedef u32 unv;
#elif __WORDSIZE==64
typedef s64 snv;
typedef u64 unv;
#endif

/* String types */
typedef u16 mwchar; // UNICODE

/* Other type definitions for clarity */
#ifndef bool
typedef u8 bool;

#ifndef true
#define true 1
#endif
#ifndef false
#define false 0
#endif

#endif

#ifndef null
#define null 0L
#endif

#ifndef NULL
#define NULL 0L
#endif

#endif
