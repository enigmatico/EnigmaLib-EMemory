CC = gcc
AR = ar
CFLAGS = -c -g -O2 -Wall
TARGET = libenigmalib.a
DEST = /usr/lib/
HDEST = /usr/include/enigmalib/
OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(AR) -rcs $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) $^ -o $@

install:
	if ! [ -d "$(HDEST)" ]; then mkdir $(HDEST); fi
	cp *.h $(HDEST)
	cp $(TARGET) $(DEST)

uninstall:
	rm -rf $(HDEST)
	rm -rf $(DEST)$(TARGET)

clean:
	rm $(TARGET) $(OBJECTS)
