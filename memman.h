#ifndef __MEM_MANAGER_H__11
#define __MEM_MANAGER_H__11

#include "ememory.h"

typedef struct __memblock__
{
	void* memh;
	unv size;
}(MMEMBLOCK);

void mm_coredump(void);
int mm_init(void);
unv mm_inc_mmblock(unv addsz);
int mm_free(void* _ptr);
void* mm_request(unv size, MMEMBLOCK** mmh);
void* mm_change_bsize(void* _ptr, unv size);
void* mm_realloc(void* _ptr, unv size);
void* mm_include(void* _ptr, unv size);
int mm_end(void);

#endif 
