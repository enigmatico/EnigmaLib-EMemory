## EnigmaLib  

This is a set of functions for memory management written in C. I made this project only so I could understand how memory management works, and it's not intended to be used in production. Use this code for reference only. If you need memory allocation, please look at GLIB's implementation instead, since it needs to be optimized in a specific way.

The *ememory* module is meant to work in any machine without the need of any kind of library. But of course, it's unoptimized. The rest are just a port of my *eutils* library meant to work along with this module.

This module takes a pointed to allocated memory and uses it for memory allocation (Basic malloc).