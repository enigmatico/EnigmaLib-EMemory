#include "ememory.h"

u8 *mHeap = 0L;
unv mHeapSz = 0;

/* mZerofill: Fills the memory with 0 */
int __mZerofill(char *dest, unv bSize)
{
	if(!dest)
		return -1;
	if(bSize == 0)
		return -1;
	
	// Loop through the whole memory
	
	while(bSize > 0)
	{
		// Set each byte to 0
		--bSize;
		dest[bSize] ^= dest[bSize];
	}
	return 0;
}

/* mMemfill: Fills the memory with a value */
int __memfill(char* dest, u8 val, unv bSize)
{
	if(!dest)
		return -1;
	if(bSize == 0)
		return -1;
	
	// Loop through the whole memory
	while(bSize > 0)
	{
		// Set each byte to val
		--bSize;
		dest[bSize] = val;
	}
	return 0;
}

/* - The following functions are unoptimized - */
/* mCopy: Copy a block of memory without having overlap into account */
int __mCopy(char* dest, char* sour, unv bSize)
{
	if(!dest || !sour)
		return -1;
	if(bSize < 1)
		return -2;
	if(dest == sour)
		return 0;
	
	
	// Loop through the whole memory
	while(bSize > 0)
	{
		// Copy each byte from source to destination
		--bSize;
		dest[bSize] = sour[bSize];
	}
	return 1;
}

/* mMove: Same as mCpy but having overlapping into account. */
int __mMove(char* dest, char* sour, unv bSize)
{
	// Dumb checks
	if(!dest || !sour)
		return -1;
	if(bSize < 1)
		return -2;
	if(dest == sour)
		return 0;
	
	
	
	if(sour < dest)
	{
		while(bSize > 0)
		{
			// Copy each byte from source to destination
			--bSize;
			dest[bSize] = sour[bSize];
		}
	} else if (sour > dest)
	{
		while(bSize > 0)
		{
			// Copy each byte from source to destination
			*dest = *sour;
			dest++;
			sour++;
			bSize--;
		}
	}
	return 1;
}
/* - - - */

/* memfill wrapper */
int eFillMem(void *dest, u8 val, unv bSize)
{
	return __memfill((char*)dest,val,bSize);
}

/* mZerofill wrapper */
int eZeroMem(void *dest, unv bSize)
{
	return __mZerofill((char*)dest, bSize);
}

/* mCopy wrapper */
int eMemCopy(void *dest, void *src, unv bSize)
{
	return __mCopy((char*)dest, (char*)src, bSize);
}

/* mMove wrapper */
int eMemMove(void *dest, void *src, unv bSize)
{
	return __mMove((char*)dest, (char*)src, bSize);
}

/* eInitHeap: initializes the heap */
int eInitHeap(void *hptr, unv hsize)
{
	if(!hptr)
		return -1;
	
	// The heap must have at least enough space for the header and a number
	if(hsize < __WORDSIZE+16)
		return -2;
	
	// Set the pointer to the heap
	mHeap = hptr;
	
	// Set the heap size
	mHeapSz = hsize;
	
	// Sets the memory to 0. Return if fails
	if(eZeroMem(mHeap, hsize) < 0)
		return -1;
	
	// Initialize the heap by placing a huge block of memory
	struct mBlockHeader *bInitHeader = (struct mBlockHeader *)mHeap;
	// Set as free memory
	bInitHeader->flag = 0;
	
	bInitHeader->magicw = _MEM_MAGICWORD_; 
	
	// Take all the heap
	bInitHeader->size = mHeapSz - sizeof(struct mBlockHeader);
	// First and only block (Both next and previous points to the start)
	bInitHeader->ptrPrevious = (struct mBlockHeader *)mHeap;
	bInitHeader->ptrNext = (struct mBlockHeader *)mHeap;
	
	return 0;
}

/* checkBlock: Check if a memory block is not corrupted */
int eCheckBlock(void *bMem)
{
	/* Null pointer check */
	if(!bMem)
		return -1;
	
	/* Check if the given block is outside of the heap */
	if((u8*)bMem < mHeap + sizeof(struct mBlockHeader) || (u8*)bMem > (mHeap + mHeapSz) - 1)
		return -2;
	
	/* Get the header */
	struct mBlockHeader *mBlock = (struct mBlockHeader *)bMem - 1;
	
	/* If the magic word is destroyed and the block is in use, it's corrupted. */
	if(mBlock->magicw != _MEM_MAGICWORD_ && mBlock->flag > 0)
		return -3;
	
	/* The size of a block can not be 0. */
	if(mBlock->size == 0)
		return -4;
	
	/* The previous and next blocks must be in the heap. */
	if(mBlock->ptrPrevious < (struct mBlockHeader *)mHeap || mBlock->ptrPrevious > (struct mBlockHeader *)mHeap + mHeapSz)
		return -5;
	
	if(mBlock->ptrNext < (struct mBlockHeader *)mHeap || mBlock->ptrNext > (struct mBlockHeader *)mHeap + mHeapSz)
		return -6;
	
	/* If the flag is 0, the block was freed or merged.
	   Yes, this check goes here. */
	if(mBlock->flag == 0)
		return 2;
	
	/* The block is healthy. */
	return 1;
}

/* checkHeap: Check the integrity of the memory heap */
int eCheckHeap()
{
	/* Check for null pointer */
	if(!mHeap)
		return -10;
	
	/* Check if the size is big enough */
	if(mHeapSz < __WORDSIZE+16)
		return -11;
	
	/* Pointer to the first block */
	struct mBlockHeader * blockPtr = (struct mBlockHeader *)mHeap;
	/* Block status */
	int bStatus = 0;
	
	/* Scan the memory */
	do
	{
		/* Perform a integrity check on the block */
		bStatus = eCheckBlock(blockPtr->mBlock);
		/* Next block */
		blockPtr = blockPtr->ptrNext;
	/* Loop */
	}while(bStatus > 0 && blockPtr->ptrNext != (struct mBlockHeader *)mHeap);
	
	/* Anything above 0 is healthy */
	if(bStatus > 1)
		return 1;
	
	/* Return the status */
	return bStatus;
}

/* mAllocMem: Allocate memory in the heap */
void *__mAllocMem(snv bSize, unv *rSize, u8 zero)
{
	// If there is a pointer to rSize, set its value first.
	if(rSize)
		*rSize = 0;
	
	// Check if the requested size exceeds the heap size
	if(bSize > mHeapSz - sizeof(struct mBlockHeader))
	{
		return 0L;
	}
	
	// Heap Block selector
	struct mBlockHeader *bHeapBlock = (struct mBlockHeader *)mHeap;
	// Previous heap block
	struct mBlockHeader *previous = bHeapBlock;
	// Split heap block
	struct mBlockHeader *split = 0L;
	
	// Align the memory to 16 bytes
	if(bSize & 3)
		bSize = ((bSize>>2)+1)<<2;
	
	// Loop through heap blocks
	for(;;)
	{
		// Check if the block is in use
		if(bHeapBlock->flag)
		{
			// Check the block integrity
			if(bHeapBlock->magicw != _MEM_MAGICWORD_)
			{
				// The memory is corrupted.
				return 0L;
			}
			// Check if this is not the last block
			if(bHeapBlock->ptrNext && bHeapBlock->ptrNext != (struct mBlockHeader *)mHeap)
			{
				// Jump to the next block (Also set this block as the previous block)
				previous = bHeapBlock;
				bHeapBlock = bHeapBlock->ptrNext;
			}else{
				// There is no free memory available
				return 0L;
			}
		}else{
			// - The block is not in use
			// Check if this block is big enough
			if(bHeapBlock->size >= bSize)
			{
				// If the block is just big enough, do not split. Use it instead.
				// This avoids fragmentation.
				if(bHeapBlock->size - bSize <= (sizeof(struct mBlockHeader)<<1) + __WORDSIZE)
				{
					bHeapBlock->flag = 1;
					bHeapBlock->magicw = _MEM_MAGICWORD_;
					// If there is a pointer to rSize, set its value.
					if(rSize)
						*rSize = bHeapBlock->size;
					// Return the pointer to the data
					return bHeapBlock->mBlock;
				}else{
					// If the block is too big, split it in two, and set the other
					// part as free.
					split = (struct mBlockHeader *)(bHeapBlock->mBlock + bSize);
					split->flag ^= split->flag;
					split->size = bHeapBlock->size - (sizeof(struct mBlockHeader) + bSize);
					split->ptrPrevious = bHeapBlock;
					split->ptrNext = bHeapBlock->ptrNext;
					bHeapBlock->flag = 1;
					bHeapBlock->magicw = _MEM_MAGICWORD_;
					bHeapBlock->size = bSize;
					bHeapBlock->ptrNext = split;
					bHeapBlock->ptrPrevious = previous;
					// If there is a pointer to rSize, set its value.
					if(rSize)
						*rSize = bHeapBlock->size;
					
					if(zero)
						__mZerofill((char*)bHeapBlock->mBlock, bSize);
					
					// Return the pointer to the data
					return bHeapBlock->mBlock;
				}
			}else{
				// The block was not big enough.
				//Check if this is the last block
				if(bHeapBlock->ptrNext != (struct mBlockHeader *)mHeap)
				{
					// If not, move to the next
					previous = bHeapBlock;
					bHeapBlock = bHeapBlock->ptrNext;
					continue;
				}else{
					// There is no free memory available
					return 0L;
				}
			}
		}
	}
}

/* mFreeMem: Frees a memory block. */
int eFreeMem(void *mBlock)
{
	if(!mBlock)
		return -1;
	// Check if the given pointer is inside the heap
	if((u8*)mBlock < mHeap || (u8*)mBlock > mHeap + mHeapSz)
	{
		return -2;
	}
	
	// Read the block structure
	struct mBlockHeader *bHead = (struct mBlockHeader *)(mBlock - sizeof(struct mBlockHeader));
	struct mBlockHeader *bNext = 0L;
	
	// Check if the block is in use. If not, return double-free error.
	if(!bHead->flag)
		return -3;
	
	// Check the block integrity. Return memory corrupted error if fails.
	if(bHead->magicw != _MEM_MAGICWORD_)
		return -4;
	
	// Unset the used flag.
	bHead->flag ^= bHead->flag;
	
	// Check if this is not the last block
	if(bHead->ptrNext != (struct mBlockHeader *)mHeap)
	{
		// Check if the next block is not in use
		bNext = bHead->ptrNext;
		if(!bNext->flag)
		{
			// Merge with the next block
			bHead->size += bNext->size + sizeof(struct mBlockHeader);
			bHead->ptrNext = bNext->ptrNext;
			// Merged blocks have the magic word destroyed
			bNext->magicw ^= bNext->magicw;
			bNext->ptrPrevious = bHead;
		}
	}
	
	// Check if this is not the first block
	if(bHead != (struct mBlockHeader *)mHeap)
	{
		// Check if the previous block is not in use
		bNext = bHead->ptrPrevious;
		if(!bNext->flag)
		{
			// Merge with the previous block
			bNext->magicw = _MEM_MAGICWORD_;
			bNext->size += bHead->size + sizeof(struct mBlockHeader);
			bNext->ptrNext = bHead->ptrNext;
			bHead->flag ^= bHead->flag;
			// Merged blocks have the magic word destroyed
			bHead->magicw ^= bHead->magicw;
			bHead = bNext;
		}
	}
	
	return bHead->flag;
}

/* Function wrappers for mAllocMem and mCallocMem */
void *eAlloc(snv bSize, unv *rSize)
{
	return __mAllocMem(bSize, rSize, 0);
}
void *eCalloc(snv bSize, unv *rSize)
{
	return __mAllocMem(bSize, rSize, 1);
}

/* mReAllocMem: Reallocates a memory block with a different size */
void *eReallocMem(void *ptr, snv bSize, unv *rSize)
{
	/* Dumb checks */
	if(!ptr)
		return 0L;
	
	if(bSize < 1)
		return 0L;
	
	/* Check if the block is healthy */
	if(eCheckBlock(ptr) < 1)
		return 0L;
	
	/* Free the memory */
	if(eFreeMem(ptr) < 0)
		return 0L;
	
	/* Allocate a new block of memory */
	return __mAllocMem(bSize, rSize, 0);
}

/* mReCallocMem: Same as mReAllocMem, but initializes to 0 */
void *eReCallocMem(void *ptr, snv bSize, unv *rSize)
{
	/* Dumb checks */
	if(!ptr)
		return 0L;
	
	if(bSize < 1)
		return 0L;
	
	/* Check if the block is healthy */
	if(eCheckBlock(ptr) < 1)
		return 0L;
	
	/* Free the memory */
	if(eFreeMem(ptr) < 0)
		return 0L;

	/* Allocate a new block of memory */
	return __mAllocMem(bSize, rSize, 1);
}
