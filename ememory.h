#ifndef _eutils_ememory_h_
#define _eutils_ememory_h_

#include "etypes.h"

#define _MEM_MAGICWORD_ 0xEFBE

struct mBlockHeader {
	u16 flag;
	u16 magicw;
	unv size;
	struct mBlockHeader *ptrPrevious;
	struct mBlockHeader *ptrNext;
	u8 mBlock[0];
};

int eFillMem(void *dest, u8 val, unv bSize);
int eZeroMem(void *dest, unv bSize);
int eMemCopy(void *dest, void *src, unv bSize);
int eMemMove(void *dest, void *src, unv bSize);
int eInitHeap(void *hptr, unv hsize);
int eCheckBlock(void *bMem);
int eCheckHeap();
int eFreeMem(void *mBlock);
void *eAlloc(snv bSize, unv *rSize);
void *eCalloc(snv bSize, unv *rSize);
void *eReallocMem(void *ptr, snv bSize, unv *rSize);
void *eReCallocMem(void *ptr, snv bSize, unv *rSize);

#endif
