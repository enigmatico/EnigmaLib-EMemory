#ifndef __STACK_MANAGER_H__11
#define __STACK_MANAGER_H__11

#include "memman.h"
#include "ememory.h"

typedef struct __stack_block__
{
	unv szBlock; // Bytes
	unv maxSize; // Blocks
	unv items; // Blocks
	int status;
	void* mmBlock;
}(STACK);

int st_errno;
char erdesc[256];

int st_init();
int st_end();
int st_endall();
STACK st_request(unv bSize, unv blocks);
unv st_push(STACK* stBlock, void* element);
int st_pop(STACK* stBlock, void* rDat);
int st_shift(STACK* stBlock, void* element);
int st_unshift(STACK* stBlock, void* rdat);
int st_reverse(STACK* stBlock);
void* st_peek(STACK* stBlock, unv elem);
void* st_poke(STACK* stBlock, unv elem, void* val);
int st_free(STACK* stBlock);
#endif
