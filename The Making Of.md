# So I wrote my own memory manager in C...

Those who follow me on Mastodon already know about this project. A few days ago, I took a look into Pokemon Emerald. There is a very nice disassembly of the game that is being ported to C in Github ([You can check it out here](https://github.com/pret/pokeemerald)).

As I usually do with these projects, the first thing I did was to check if the game could be reassembled. And it wasn't too hard to do, I only had to download and ARM dev kit with the whole GCC toolchain written specificaly for ARM, then a specific tool I had to recompile and copy into the project, and soon enough it was compiling into a working rom!

That's when I started fiddling with the code. The people that did the disassembly did a really impressive job and not only they disassembled the code in a very comprehensible way, they also ported most of it into C! And since C is my main language, understanding the code isn't complicated.

First I took a look into some "trivial" stuff like how attacks works in combat. And very soon I made it so every pokemon was disobedient!

![TORCHIC is loafing around](https://cdn.niu.moe/media_attachments/files/001/308/286/original/d8cc869967cf50c8.png)![Even wild POKEMON are disobedient](https://cdn.niu.moe/media_attachments/files/001/308/288/original/04d3ac73995de486.png)

I made a few more modifications to see if I could understand the pieces of the code I was studying. That's when I started fiddling with the dynamic memory in Pokemon. The memory management is handled in [malloc.c](https://github.com/pret/pokeemerald/blob/master/src/malloc.c) and is initialized in AgbMain, which is the main entry point of the game ([main.c](https://github.com/pret/pokeemerald/blob/master/src/main.c)).

## Understanding malloc.c

The memory management in this game is very simplistic, which is to be expected from a Game Boy Advance game from 13 years ago. But it's quite efficient in what it does for the limited amount of resources it has.

The heap is basically a pointer to a static part of the memory in the console that is used for general purpose  applications (0x2000000), and has a fixed size. So the heap is basically static memory, as it's to be expected. The game manages this memory by dividing it into blocks or regions of an specific size. These blocks are described by a memory structure that is exactly 16 bytes long in memory.

When the heap is initialized, it creates a large block of memory that fits the whole heap. Then, when the game request a portion of this memory using the Alloc function (which takes an unsigned 32-bit ingteger as the size paramenter), it splits this chunk of memory into two parts. One that is at least the requested size (plus the header), and another free block.  

The game will loop through the blocks in the heap until a free block is found. If it is big enough, then it will decide wether it should split it (if it's too big), or simply use it (if it's just big enough). This of course causes a lot of [fragmentation in the memory](https://en.wikipedia.org/wiki/Memory_fragmentation).

Oddly enough, the function in charge of easing the fragmentation is the FreeInternal function. This is the function in charge of freeing the blocks of memory, which is a really simplistic function as it only has to unset the used flag in the block, and it's considered free. Pretty much like most file systems do something similar to declare sectors as free when "deleting" files.

After doing this, the game then checks if the adjacent blocks are also free or not. If they are, it merges them into one big block by changing the headers. What intrigues me is that it only does this once. It doesnt look if there are more adjacent blocks unused. I assume this is for CPU speed optimizations, as the game won't need to do much allocation anyways, and this small solution is enough to ease the fragmentation in the memory.

And that's pretty much everything that there is to know about this module. There are functions to check if the memory blocks are not corrupted, and the rest are just wrappers or intermediate functions to handle headers. It is extremely simplistic! And for a game, it works well enough.  

If you look at the memory in any emulator at address 0x2000000, you will see the first allocated block of memory and it's header (Notice the 0xA3A3 magic word). Following the structure defined in malloc.c (struct MemBlock) you can see each member in the memory: flag (2bytes), magic word (2bytes, 0xA3A3), size (4bytes), pointer to previous memory (4bytes, its set to 0x2000000), and pointer to next memory (4bytes, set  to 0x2000810).

![Memory disassembly of the first block of memory](https://cdn.niu.moe/media_attachments/files/001/310/885/original/4cb2e01c60aa61a6.png)

If you go to the next block (In this case 0x2000810) you can see the structure of the next block. Pretty much the same. And so on!  Until the last block (We know it's the last when next points to the start of the heap, 0x2000000).

![The next block](https://cdn.niu.moe/media_attachments/files/001/310/887/original/7a0a1d527bc55373.png)  

## Writting my own implementation

In order to know if I understood all of this correctly, I made my own implementation in C, for regular x86_64. And as an extra challenge, I wanted to do it without using any standard library function (aside from printf for debugging purposes only). And this challenge gave me some extra problems I could resolve easily.

The first problem was **type sizes**. The structure in the game works well for a 32-bit ARM processor, but I had an extra problem which is the fact that I was compiling for x86 AND x86_64 (32 and 64 bits). Which means that the pointers are bigger in 64-bits (8 bytes instead of 4). And so, the headers would surpass the 16 bytes limit (which is a good size for alignment).

I couldn't use inttypes.h or limits.h or any C standard type as I am not using the standard library (aside from int and every other standard type not defined anywhere). So I had to make my own types.

What I did was to take a small set of preprocessor instructions from wordsize.h, that defines a set of constants depending on wheter __x86_64__ or __ILP32__ are defined when compiling the program. And from there, I could detect what kind of types should I be using for my own definitions.

```c
/* TAken from wordsize.h */
#if defined __x86_64__ && !defined __ILP32__
# define __WORDSIZE        64
#else
# define __WORDSIZE        32
#define __WORDSIZE32_SIZE_ULONG                0
#define __WORDSIZE32_PTRDIFF_LONG        0
#endif
#ifdef __x86_64__
# define __WORDSIZE_TIME64_COMPAT32        1
/* Both x86-64 and x32 use the 64-bit system call interface.  */
# define __SYSCALL_WORDSIZE                64
#else
# define __WORDSIZE_TIME64_COMPAT32        0
#endif
```  

```c
/* Signed types
 * (s = signed, u = unsigned, 8/16/32/64 number of bits )*/
typedef signed char s8;
typedef short int s16;
typedef int s32;

/* Unsigned types */
typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;

/* 64-bit types */
#if __WORDSIZE == 64
typedef long int s64;
typedef unsigned long int u64;
#else
typedef long long int s64;
typedef unsigned long long int u64;
#endif

/* snv and unv have a size equal to the CPU architecture size 
 * (s = signed, u = unsigned, n = numeric, v = value )*/
#if __WORDSIZE==32
typedef s32 snv;
typedef u32 unv;
#elif __WORDSIZE==64
typedef s64 snv;
typedef u64 unv;
#endif
```  

The snv and unv types will save me to have to define two different structures for 32 and 64 bits.

The next problem to solve is the memory copy and memory filling functions, but these are not really a problem. Take two pointers and just copy each byte into the other using a loop (or set to 0 or whatever). This doesnt have into account memory overlapping (when one of the memory regions is inside the other) but for this case, it works.

```c
/* mCopy: Copy a block of memory without having overlap into account */
int mCopy(void* dest, void* sour, unv bSize)
{
	if(!dest)
		return -1;
	if(!sour)
		return -1;
	if(bSize == 0)
		return -1;
	
	// This is to avoid "ILLEGAL USE OF VOID"
	u8* cDest = (u8*)dest;
	u8* cSour = (u8*)sour;
	
	// Loop through the whole memory
	while(bSize > 0)
	{
		// Copy each byte into the destination
		cDest[bSize-1] = cSour[bSize-1];
		bSize--;
	}
	return 0;
}  
```

With these problems already solved, I could start writting my memory manager. First I made a memory heap, which is just a statically allocated block of memory. Then the structure for the headers, which is similar to the one in the game, but it will be 16 or 32 bytes long depending on the target architecture.

```c
#define _MEM_MAGICWORD_ 0xEFBE
#define _HEAP_SIZE_ 262144

u8 mHeap[_HEAP_SIZE_];

struct mBlockHeader {
	u16 flag;
	u16 magicw;
	unv size;
	struct mBlockHeader *ptrPrevious;
	struct mBlockHeader *ptrNext;
	u8 mBlock[0];
};
```  

Then the heap initialization function, which simply fills the heap with zero, and then creates a huge block that fits the whole heap.

```c
/* mInitHeap: initializes the heap */
int mInitHeap()
{
	// Sets the memory to 0
	int zfres = mZerofill(mHeap, _HEAP_SIZE_);
	// If this fails, return error
	if(zfres < 0)
		return -1;
	
	// Initialize the heap by placing a huge block of memory
	struct mBlockHeader *bInitHeader = (struct mBlockHeader *)mHeap;
	// Set as free memory
	bInitHeader->flag = 0;
	
	bInitHeader->magicw = _MEM_MAGICWORD_; 
	
	// Take all the heap
	bInitHeader->size = _HEAP_SIZE_ - sizeof(struct mBlockHeader);
	// First and only block (Both next and previous points to the start)
	bInitHeader->ptrPrevious = (struct mBlockHeader *)mHeap;
	bInitHeader->ptrNext = (struct mBlockHeader *)mHeap;
	return 0;
}
```  

The allocation and free functions are slightly more complex than the ones in the game. The allocation function does pretty much the same as the one in the game. However, it has some extra checks for corrupted blocks of memory. If the memory is corrupted, it won't allocate in that space. In fact, it won't allocate at all since that means there is a huge problem in your program. The rest is the same, it aligns the memory, then it loops through each block until it finds a big enough space, and proceeds to either take it, or split it first. If no free space is found, it returns a null pointer.  

```c
/* mAllocMem: Allocate memory in the heap */
void *mAllocMem(snv bSize, int *rSize)
{
	// If there is a pointer to rSize, set its value first.
	if(rSize)
		*rSize = 0;
	
	// Check if the requested size exceeds the heap size
	if(bSize > _HEAP_SIZE_ - sizeof(struct mBlockHeader))
	{
		return 0L;
	}
	
	// Heap Block selector
	struct mBlockHeader *bHeapBlock = (struct mBlockHeader *)mHeap;
	// Previous heap block
	struct mBlockHeader *previous = (struct mBlockHeader *)mHeap;
	// Split heap block
	struct mBlockHeader *split = 0L;
	
	// Align the memory to 16 bytes
	if(bSize & 3)
		bSize = ((bSize>>2)+1)<<2;
	
	// Loop through heap blocks
	for(;;)
	{
		// Check if the block is in use
		if(bHeapBlock->flag)
		{
			// Check the block integrity
			if(bHeapBlock->magicw != _MEM_MAGICWORD_)
			{
				// The memory is corrupted.
				return 0L;
			}
			// Check if this is not the last block
			if(bHeapBlock->ptrNext && bHeapBlock->ptrNext != (struct mBlockHeader *)mHeap)
			{
				// Jump to the next block (Also set this block as the previous block)
				previous = bHeapBlock;
				bHeapBlock = bHeapBlock->ptrNext;
			}else{
				// There is no free memory available
				return 0L;
			}
		}else{
			// - The block is not in use
			// Check if this block is big enough
			if(bHeapBlock->size >= bSize)
			{
				// If the block is just big enough, do not split. Use it instead.
				// This avoids fragmentation.
				if(bHeapBlock->size - bSize <= (sizeof(struct mBlockHeader)<<1) + __WORDSIZE)
				{
					bHeapBlock->flag = 1;
					bHeapBlock->magicw = _MEM_MAGICWORD_;
					// If there is a pointer to rSize, set its value.
					if(rSize)
						*rSize = bHeapBlock->size;
					// Return the pointer to the data
					return bHeapBlock->mBlock;
				}else{
					// If the block is too big, split it in two, and set the other
					// part as free.
					split = (struct mBlockHeader *)(bHeapBlock->mBlock + bSize);
					split->flag ^= split->flag;
					split->size = bHeapBlock->size - (sizeof(struct mBlockHeader) + bSize);
					split->ptrPrevious = bHeapBlock;
					split->ptrNext = bHeapBlock->ptrNext;
					bHeapBlock->flag = 1;
					bHeapBlock->magicw = _MEM_MAGICWORD_;
					bHeapBlock->size = bSize;
					bHeapBlock->ptrNext = split;
					bHeapBlock->ptrPrevious = previous;
					// If there is a pointer to rSize, set its value.
					if(rSize)
						*rSize = bHeapBlock->size;
					// Return the pointer to the data
					return bHeapBlock->mBlock;
				}
			}else{
				// The block was not big enough.
				//Check if this is the last block
				if(bHeapBlock->ptrNext == (struct mBlockHeader *)mHeap)
				{
					// Check if the next block is free
					if(!bHeapBlock->ptrNext)
					{
						// Jump to the next (Also set this block as the previous block)
						previous = bHeapBlock;
						bHeapBlock = bHeapBlock->ptrNext;
					}else{
						// There is no free memory available
						return 0L;
					}
				}else{
					// There is no free memory available
					return 0L;
				}
			}
		}
	}
}  
```

As for the memory free function, it's the same, but it also checks if the given pointer is inside of the heap. And also has memory corruption checks, as well as checks for double-free. The rest is the same, it unsets the used flag, then merges the adjacent free blocks.  

```c
/* mFreeMem: Frees a memory block. */
int mFreeMem(void *mBlock)
{
	if(!mBlock)
		return -1;
	// Check if the given pointer is inside the heap
	if((u8*)mBlock < mHeap || (u8*)mBlock > mHeap + _HEAP_SIZE_)
		return -2;
	
	// Read the block structure
	struct mBlockHeader *bHead = (struct mBlockHeader *)(mBlock - sizeof(struct mBlockHeader));
	struct mBlockHeader *bNext = 0L;
	
	// Check if the block is in use. If not, return double-free error.
	if(!bHead->flag)
		return -3;
	
	// Check the block integrity. Return memory corrupted error if fails.
	if(bHead->magicw != _MEM_MAGICWORD_)
		return -4;
	
	// Unset the used flag.
	bHead->flag ^= bHead->flag;
	
	// Check if this is not the last block
	if(bHead->ptrNext != (struct mBlockHeader *)mHeap)
	{
		// Check if the next block is not in use
		bNext = bHead->ptrNext;
		if(!bNext->flag)
		{
			// Merge with the next block
			bHead->size += bNext->size + sizeof(struct mBlockHeader);
			bHead->ptrNext = bNext->ptrNext;
			// Merged blocks have the magic word destroyed
			bNext->magicw ^= bNext->magicw;
			bNext->ptrPrevious = bHead;
		}
	}
	// Check if this is not the first block
	if(bHead != (struct mBlockHeader *)mHeap)
	{
		// Check if the previous block is not in use
		bNext = bHead->ptrPrevious;
		if(!bNext->flag)
		{
			// Merge with the previous block
			bNext->magicw = _MEM_MAGICWORD_;
			bNext->size += bHead->size + sizeof(struct mBlockHeader);
			bNext->ptrNext = bHead->ptrNext;
			bHead->flag ^= bHead->flag;
			// Merged blocks have the magic word destroyed
			bHead->magicw ^= bHead->magicw;
			bHead = bNext;
		}
	}
	
	return bHead->flag;
}  
```  

## The moment of truth...

I made many tests to polish this code, and although there might be some minor bugs at some point, I can say it works. It at least did what it's supposed to do when I tried allocating and deallocating different blocks of memory. It splits free blocks, it merges unused blocks when freeing them, and uses entire blocks when the space is just enough (I left an additional margin of 32/64 bytes of memory for the split to happen), as well as reusing free blocks. If there is not enough memory, it will return a null pointer. Otherwise, I do get a pointer I can use.

![Some tests](https://a.uguu.se/OD6Mu1zk8fuG.png)

Obviously this is not a very efficient memory management system for very memory intensive programs, but as a proof of concept it’s very nice, and it was fun to write it.