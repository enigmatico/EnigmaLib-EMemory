#include "memman.h" 

MMEMBLOCK** mmblock_ptr;
unv mptr_sz = 0;
unv mptr_pt = 0;
char mm_session = 0x0;

int mm_init()
{
	if(mm_session)
		return 0;
	mmblock_ptr = (MMEMBLOCK**)eCalloc(sizeof(MMEMBLOCK*),null);
	if(!mmblock_ptr)
		return 0;
	mptr_sz = 1;
	mptr_pt = 0;
	mm_session = 0x1;
	return 1;
}

unv mm_inc_mmblock(unv addsz)
{
	unv oldsize = mptr_sz*sizeof(MMEMBLOCK*);
	unv newsize = (mptr_sz+addsz)*sizeof(MMEMBLOCK*);
	MMEMBLOCK** mmblock_tmp = (MMEMBLOCK**)eCalloc(mptr_sz * sizeof(MMEMBLOCK**), null);
	eMemMove(mmblock_tmp,mmblock_ptr,oldsize);
	mmblock_ptr = (MMEMBLOCK**)eReallocMem(mmblock_ptr, newsize, null);
	eFillMem(mmblock_ptr,0x0,newsize);
	eMemMove(mmblock_ptr,mmblock_tmp,oldsize);
	eFreeMem(mmblock_tmp);
	mptr_sz += addsz;
	return mptr_sz;
}

void* mm_change_bsize(void* _ptr, unv size)
{
	unv pos = 0;
	if(!_ptr)
		return NULL;
	for(pos = 0; pos <= mptr_pt; pos++)
	{
		if(pos == mptr_pt)
			return NULL;
		if(mmblock_ptr[pos])
		{
			if(mmblock_ptr[pos]->memh == _ptr)
			{
				break;
			}
		}
	}

	void* temp = eCalloc(size, null);
	if(!temp)
		return NULL;
	if(size < mmblock_ptr[pos]->size)
	{
		eMemMove(temp,_ptr,size);
	}else{
		eMemMove(temp,_ptr,mmblock_ptr[pos]->size);
	}
	eFreeMem(mmblock_ptr[pos]->memh);
	mmblock_ptr[pos]->memh = temp;
	mmblock_ptr[pos]->size = size;
	return temp;
}

void* mm_realloc(void* _ptr, unv size)
{
	unv pos = 0;
	if(!_ptr)
		return 0x0;
	for(pos = 0; pos <= mptr_pt; pos++)
	{
		if(pos == mptr_pt)
			return 0x0;
		if(mmblock_ptr[pos])
		{
			if(mmblock_ptr[pos]->memh == _ptr)
			{
				break;
			}
		}
	}

	void* temp = eCalloc(size, null);
	if(!temp)
		return NULL;
	eFreeMem(mmblock_ptr[pos]->memh);
	mmblock_ptr[pos]->memh = temp;
	mmblock_ptr[pos]->size = size;
	return temp;
}

int mm_free(void* _ptr)
{
	unv newsize = (mptr_sz-1)*sizeof(MMEMBLOCK*);
	unv pos = 0, second = 0;
	char skip = 0;
	if(!_ptr)
		return -1;
	for(pos = 0; pos <= mptr_pt; pos++)
	{
		if(pos == mptr_pt)
			return -2;
		if(mmblock_ptr[pos])
		{
			if(mmblock_ptr[pos]->memh == _ptr)
			{
				eFreeMem(mmblock_ptr[pos]->memh);
				mmblock_ptr[pos]->memh = NULL;
				
				break;
			}
		}

	}

	if(mmblock_ptr[pos] != NULL)
	{
		eFreeMem(mmblock_ptr[pos]);
	}
	MMEMBLOCK** mmblock_tmp = (MMEMBLOCK**)eCalloc((mptr_sz-1) * sizeof(MMEMBLOCK**), null);
	for(second = 0; second < mptr_sz; second++)
	{
		if(second == pos)
		{
			skip++;
			continue;
		}
		if(!skip)
		{
			mmblock_tmp[second] = mmblock_ptr[second];
		}else{
			mmblock_tmp[second-1] = mmblock_ptr[second];
		}
	}
	mmblock_ptr = (MMEMBLOCK**)eReallocMem(mmblock_ptr, newsize, null);
	eMemMove(mmblock_ptr,mmblock_tmp,newsize);
	eFreeMem(mmblock_tmp);
	mptr_sz--;
	mptr_pt--;
	return 0;
}

void* mm_request(unv size, MMEMBLOCK** mmh)
{
	if(size < 1)
	{
		return NULL;
	}
	MMEMBLOCK* block_hnd = (MMEMBLOCK*)eCalloc(sizeof(MMEMBLOCK), null);
	void* memblock = eCalloc(size, null);
	if(!memblock || !block_hnd)
		return NULL;
	block_hnd->memh = memblock;
	block_hnd->size = size;
	mmblock_ptr[mptr_pt] = block_hnd;
	if(mmh != NULL)
		*mmh = block_hnd;
	mm_inc_mmblock(1);
	mptr_pt++;
	return memblock;
}

void* mm_include(void* _ptr, unv size)
{
	if(!_ptr || size == 0)
		return NULL;
	MMEMBLOCK* block_hnd = (MMEMBLOCK*)eCalloc(sizeof(MMEMBLOCK), null);
	if(!block_hnd)
		return NULL;
	block_hnd->memh = _ptr;
	block_hnd->size = size;
	mmblock_ptr[mptr_pt] = block_hnd;
	mm_inc_mmblock(1);
	mptr_pt++;
	return mmblock_ptr[mptr_pt-1]->memh;
}

int mm_end()
{
	unv x = 0;
	for(x = 0; x < mptr_pt; x++)
		if(mmblock_ptr[x] != NULL)
		{
			if(mmblock_ptr[x]->memh != NULL)
				eFreeMem(mmblock_ptr[x]->memh);
			eFreeMem(mmblock_ptr[x]);
		}

	eFreeMem(mmblock_ptr);
	mmblock_ptr = NULL;
	mm_session = 0x0;
	mptr_sz = 0;
	mptr_pt = 0;
	return 0;
}
