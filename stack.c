#include "stack.h" 

void** ltStacks = NULL;
unv ltSize = 0;
unv nStacks = 0;
u8 stInit = 0;
int st_errno = 0;
char erdesc[256] = { 0x0 };

int st_init()
{
	mm_init();
	if(stInit)
		return 0;

	ltStacks = (void*)mm_request(sizeof(void*), NULL);

	if(!ltStacks)
		return 0;

	ltSize = 1;
	nStacks = 0;
	stInit = 0x30;
	return 1;
}

STACK st_request(unv bSize, unv blocks)
{
	STACK nSt;
	nSt.status = 0;
	//memset(&nSt, 0x0, sizeof(nSt));
	eZeroMem(&nSt, sizeof(nSt));
	//unv sNum = 0;

	if(!stInit)
	{
		nSt.status = -1;
		return nSt;
	}

	if(bSize < 1 || blocks < 1)
	{
		nSt.status = -2;
		return nSt;
	}

	
	nSt.szBlock = bSize;
	nSt.mmBlock = mm_request(bSize*blocks, NULL);

	if(!nSt.mmBlock)
	{
		nSt.status = -3;
		return nSt;
	}

	nSt.items = 0;
	nSt.maxSize = blocks;

	//sNum = nStacks;

	int emptySlot = -1;
	for(int x = 0; x < ltSize-1; x++)
	{
		if(!ltStacks[x])
		{
			ltStacks[x] = nSt.mmBlock;
			emptySlot = x;
		}
	}
	if(emptySlot < 0)
	{
		ltStacks[nStacks] = nSt.mmBlock;
		nStacks++;
		ltSize++;
		ltStacks = mm_change_bsize(ltStacks, ltSize*sizeof(STACK));
	}

	return nSt;
}

unv st_push(STACK* stBlock, void* element)
{
	if(!stBlock)
		return 0;
	if(!element)
		return 0;

	if(stBlock->items >= stBlock->maxSize)
	{
		stBlock->mmBlock = mm_change_bsize(stBlock->mmBlock, (stBlock->maxSize+1)*sizeof(STACK));
		stBlock->maxSize++;
	}
	eMemMove(stBlock->mmBlock+(stBlock->items*stBlock->szBlock), element,stBlock->szBlock);
	stBlock->items++;
	return stBlock->items-1;
}

int st_pop(STACK* stBlock, void* rDat)
{
	if(!stBlock)
		return -1;
	if(stBlock->items < 1)
		return -2;

	eMemMove(rDat,stBlock->mmBlock+(stBlock->szBlock*--stBlock->items), stBlock->szBlock);
	//memset(stBlock->mmBlock+(stBlock->items*stBlock->szBlock),0x0,stBlock->szBlock);
	eZeroMem(stBlock->mmBlock+(stBlock->items*stBlock->szBlock), stBlock->szBlock);
	return 1;
}

int st_shift(STACK* stBlock, void* element)
{
	if(!stBlock)
		return 0;
	if(!element)
		return 0;

	if(stBlock->items >= stBlock->maxSize)
	{
		stBlock->mmBlock = mm_change_bsize(stBlock->mmBlock, (stBlock->maxSize+1)*sizeof(STACK));
		stBlock->maxSize++;
	}
	if(stBlock->items > 0)
	{
		eMemMove(stBlock->mmBlock+stBlock->szBlock,stBlock->mmBlock,stBlock->items*stBlock->szBlock);
	}
	eMemMove(stBlock->mmBlock,element,stBlock->szBlock);
	stBlock->items++;
	return stBlock->items-1;
}

int st_unshift(STACK* stBlock, void* rdat)
{
	if(!stBlock)
		return -1;
	if(stBlock->items < 1)
		return -2;

	eMemMove(rdat,stBlock->mmBlock,stBlock->szBlock);
	eMemMove(stBlock->mmBlock,stBlock->mmBlock+stBlock->szBlock,stBlock->szBlock*(stBlock->items-1));
	//memset(stBlock->mmBlock+(--stBlock->items*stBlock->szBlock),0x0,stBlock->szBlock);
	eZeroMem(stBlock->mmBlock+(--stBlock->items*stBlock->szBlock),stBlock->szBlock);
	return 1;
}

int st_reverse(STACK* stBlock)
{
	if(!stBlock)
		return -1;

	void* tmpBlock = mm_request(stBlock->szBlock * stBlock->items, NULL);

	if(!tmpBlock)
		return -2;

	for(int x = 0; x < stBlock->items; x++)
	{
		eMemMove(tmpBlock + (x*stBlock->szBlock), stBlock->mmBlock + (stBlock->szBlock * (stBlock->items-x-1)), stBlock->szBlock);
	}

	mm_free(stBlock->mmBlock);
	stBlock->mmBlock = tmpBlock;
	return 0;
}

void* st_peek(STACK* stBlock, unv elem)
{
	st_errno = 0;

	if(!stBlock)
	{
		st_errno = 1;
		return NULL;
	}

	if(elem >= stBlock->items)
	{
		st_errno = 2;
		return NULL;
	}

	return (void*)(stBlock->mmBlock + (stBlock->szBlock * elem));
}

void* st_poke(STACK* stBlock, unv elem, void* val)
{
	st_errno = 0;

	if(!stBlock)
	{
		st_errno = 1;
		return NULL;
	}

	if(elem >= stBlock->items)
	{
		st_errno = 2;
		return NULL;
	}

	if(!val)
	{
		st_errno = 3;
		return NULL;
	}

	eMemMove(stBlock->mmBlock + (elem*stBlock->szBlock), val, stBlock->szBlock);

	return 	stBlock->mmBlock + (elem*stBlock->szBlock);
}

int st_end()
{
	if(!stInit)
		return 0;

	for(unv x = 0; x < nStacks; x++)
	{
		if(ltStacks[x])
			mm_free(ltStacks[x]);
	}

	mm_free(ltStacks);
	ltSize = 0;
	nStacks = 0;
	return 1;
}

int st_free(STACK* stBlock)
{
	if(!stInit)
		return -1;

	if(!stBlock)
		return -2;

	if(stBlock->status)
		return -3;

	for(unv x = 0; x <= nStacks; x++)
	{
		if(x >= nStacks)
			return -3;
		if(ltStacks[x] == stBlock)
		{
			if(ltStacks)
			{
				mm_free(ltStacks[x]);
			}
			ltStacks[x] = NULL;
			stBlock->mmBlock = NULL;
			stBlock->status = 1;
			stBlock->items = 0;
			stBlock->maxSize = 0;
			stBlock->szBlock = 0;
		}
	}
	return 0;
}

int st_endall()
{
	if(!stInit)
		return -1;

	for(unv x = 0; x < nStacks; x++)
	{
		if(ltStacks[x])
			mm_free(ltStacks[x]);
	}

	mm_free(ltStacks);
	ltSize = 0;
	nStacks = 0;
	mm_end();
	return 0;
}
